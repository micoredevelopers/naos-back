<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class FeedbackRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'        => ['required', 'string', 'min:2'],
			'phone'       => ['required', 'string', 'min:10'],
			'email'       => ['required', 'string', 'email'],
			'lastName'    => ['nullable', 'string', 'min:2'],
			'companyName' => ['nullable', 'string', 'min:2'],
			'position'    => ['nullable', 'string', 'min:2'],
			'website'     => ['nullable', 'string', 'min:5'],
			'message'     => ['nullable', 'string'],
			'confirm'     => ['required'],
		];
	}

	public function getFillableFields(): array
	{
		return $this->only(Arr::except(array_keys($this->rules()), ['confirm']));
	}

	public function messages()
	{
		return ['confirm.required' => 'You must agree with terms and conditions'];
	}
}
