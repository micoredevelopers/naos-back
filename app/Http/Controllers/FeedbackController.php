<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Mail\SendFeedback;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
	public function index(FeedbackRequest $request)
	{
		$data = $request->getFillableFields();
		logger($data);
		Mail::send(new SendFeedback($data));

		return ['status' => 'success', 'message' => 'Your request was success submitted'];
	}
}
