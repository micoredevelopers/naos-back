<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class SendFeedback extends Mailable
{
	use Queueable, SerializesModels;

	/**
	 * @var array
	 */
	private $data;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(array $data = [])
	{
		$this->data = $data;
		$this->to(env('APP_ENV') === 'local' ? env('DEBUG_EMAIL') : env('MAIL_TO_ADDRESS'))
			->subject(sprintf('New request from %s', (string)$data['name']))
		;
	}

	public function build()
	{
		$mail = (new MailMessage);
		foreach ($this->data as $key => $value) {
			$mail->line(Str::ucfirst($key) . ': ' . $value);
		}
		//костыль
		return $this->view('mail.feedback', ['html' => (string)$mail->render()]);
	}
}
